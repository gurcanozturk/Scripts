#!/usr/bin/python3
# Move emails in INBOX folder to Gitlab folder by sender (sender-address@mailserver.com.tr)

from exchangelib import Credentials, Account

credentials = Credentials('your_exchange_mail@exchange-server.com, 'PASSWORD')
account = Account('your_exchange_mail@exchange-server.com', credentials=credentials, autodiscover=True)

to_folder = account.inbox / 'Gitlab'

# Process 1000 mails, increase as you want.
for item in account.inbox.all().filter(sender='sender-address@mailserver.com.tr').order_by('-datetime_received')[:1000]:
   item.move(to_folder)
