#!/usr/bin/env python
#

import requests
import json
import sys
import argparse

def getOptions(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description="Parses command.")
    parser.add_argument("-o", "--operation", help="DNS Operation - create/update/delete")
    parser.add_argument("-t", "--type", help="DNS Record Type")
    parser.add_argument("-r", "--record", help="DNS Record Name")
    parser.add_argument("-d", "--domain", help="DNS Record Domain Name")
    parser.add_argument("-v", "--value", help="DNS Record Value")
    options = parser.parse_args(args)
    return options

def get_zone_id(domain):
        r = requests.get(
            url=("https://dns.hetzner.com/api/v1/zones?name=%s" % (domain)),
            headers={
                "Auth-API-Token": token,
            },
        )

        if r.status_code == 200:
          content = json.loads(r.content.decode())
          zone_id = content["zones"][0]["id"]
          return zone_id

def list_records(zone_id):

    try:
        r = requests.get(
            url="https://dns.hetzner.com/api/v1/records",
            params={
                "zone_id": zone_id,
            },
            headers={
                "Auth-API-Token": token,
            },
        )
        if r.status_code == 200:
          records = json.loads(r.content.decode())["records"]
          for record in records:
            print ("%s - %s - %s - %s" % (record["id"], record["name"], record["type"], record["value"]))

    except requests.exceptions.RequestException:
        print('HTTP Request failed')

def get_record_id(zone_id, record_name):
    try:
        r = requests.get(url="https://dns.hetzner.com/api/v1/records",
            params={
                "zone_id": zone_id,
            },
            headers={
                "Auth-API-Token": token,
            },
        )
        if r.status_code == 200:
          records = json.loads(r.content.decode())["records"]

          for record in records:
              record_name  = record["name"]
              if record_name == record_name:
                record_id  = record["id"]

          return record_id

    except requests.exceptions.RequestException:
        print('HTTP Request failed')


def create_record(type, name, zone_id, value):

    try:
        r = requests.post(
            url="https://dns.hetzner.com/api/v1/records",
            headers={
                "Content-Type": "application/json",
                "Auth-API-Token": token,
            },
            data=json.dumps({
              "value": value,
              "ttl": 86400,
              "type": type.upper(),
              "name": name,
              "zone_id": zone_id
            })
        )

        if r.status_code == 422:
          result = r.content.decode()
          message  = json.loads(result)["error"]["message"]
          print('Error\n - Response HTTP Response Body: %s' % (message))

        if r.status_code == 200:
          print('Response HTTP Response Body: {content}'.format(content=r.content))

    except requests.exceptions.RequestException:
        print('HTTP Request failed')

def delete_record(record_id):
    try:
        r = requests.delete(
            url=("https://dns.hetzner.com/api/v1/records/%s" % (record_id)),
            headers={
                "Auth-API-Token": token,
            },
        )
        if r.status_code == 422:
          result = r.content.decode()
          message  = json.loads(result)["error"]["message"]
          print('Error\n - Response HTTP Response Body: %s' % (message))

        if r.status_code == 200:
          print (r.text)
          print('Response HTTP Response Body: {content}'.format(content=r.content))

    except requests.exceptions.RequestException:
        print('HTTP Request failed')

def do_nothing():
  return True

def print_usage():
   print ("\n Usage: %s -o <create|update|delete|list> -t <record_type> -r <record_name> -d <record_domain> -v <record_value>\n" % (sys.argv[0]))

token = "YOUR_TOKEN"

options = getOptions(sys.argv[1:])

if len(sys.argv) < 1:
   print_usage()

else:
    domain = options.domain
    zone_id = get_zone_id(domain)

    if (options.operation == "list"):
       if domain:
          print (list_records(zone_id))
       else:
          print ("domain bilgisi bos olamaz");
          print_usage()
    elif (options.operation == "create"):
       if (options.type and  options.record and  zone_id and  options.value):
          create_record(options.type, options.record, zone_id, options.value)
       else:
          print ("eksik birsey mi var komutlarimda?")
          print ("%s %s %s %s" % (options.type, options.record, zone_id, options.value))
          print_usage()
    elif options.operation == "update":
       record = options.record
       record_id = get_record_id(zone_id, record)
       #update_record(record_id)
    elif (options.operation == "delete"):
       record = options.record
       record_id = get_record_id(zone_id, record)
       print (record_id)
       delete_record(record_id)
    else:
       do_nothing()
       print_usage()
