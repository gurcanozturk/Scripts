#!/usr/bin/perl

use Net::LDAP;

sub getADInfo
{
        my $targetuser = shift;
        my $ldapuser = "AD_USER";
        my $ldappassword = "AD_PASSWORD";
        my $domain = "AD_SERVER";

        my $ad = Net::LDAP->new($domain) or die "Could not connect!";

        $ad->bind($ldapuser, password=>$ldappassword);
        my $searchbase = 'dc=domain,dc=com,dc=local';
        my $filter = "mail=$targetuser";
	
        my $results = $ad->search(base=>$searchbase, filter=>$filter);
        my $count = $results->count;

        if ($count)
        {
                my @entries = $results->entries;
                foreach my $entry (@entries)
                {
                        $username = $entry->get_value('samaccountname');
                        return ($username);
                }
        }
        else
        {
                return "";
        }
        $ad->unbind;
}

open(FILE,"export.csv");
while ($line=<FILE>) {
  # "dn","cn","gidnumber","givenname","homedirectory","loginshell","objectclass","sn","uid","uidnumber","userpassword","mail","departmentnumber","gecos","mobile","description"
  ($dn, $cn, $gid, $givenName, $home, $shell, $class, $sn, $userid, $uid, $pass, $mail, $phone, $gecos, $mobile, $desc ) = split(';' , $line);

  if (index($mail, "@") ne -1) {
	my $username= &getADInfo($mail);
	print lc($userid) .":".lc($username).":".lc($mail);
	print "\n";
  }
}
