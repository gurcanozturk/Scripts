#!/bin/bash
# Shell script to connect to Fortigate SSLVPN via openfortivpn w/o GUI
#
# Requires openfortivpn : https://github.com/adrienverge/openfortivpn 
# If you're using Debian/Ubuntu based distro install through APT way
# apt install openfortivpn
#
# Configuration:
# Put your SSLVPN credentials in /home/$USER/.fortivpn.cfg
#
# host=VPN_HOST_OR_IP
# port=443 # 
# username=YOUR_USERNAME
# password=MY_VERY_SECRET_PASSWORD
# trusted-cert=SSLVPN_CERT # if you need.

VPN_EXECUTABLE=/usr/bin/openfortivpn
VPN_EXECUTABLE_PARAMS="-c/home/$USER/.fortivpn.cfg"
VPN_INTERFACE=ppp0
VPN_CONNECTED="/sbin/ifconfig | egrep -A1 $VPN_INTERFACE | grep inet"
VPN_DISCONNECT_CMD="sudo killall -2 openfortivpn"


if [ -z "$1" ]
  then
    echo "Usage: $0 connect"
    exit
fi

case "$1" in
    connect)

        FCTOKEN=$(zenity --entry --title OTP --text "Please enter your OTP token")
        $VPN_EXECUTABLE "$VPN_EXECUTABLE_PARAMS" --otp=$FCTOKEN &
        ;;
    disconnect)
            eval "$VPN_DISCONNECT_CMD"
        ;;
esac
