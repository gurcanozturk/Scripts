#!/usr/bin/python3
# add new firewall rule into Hetzner server if public_ip changes
#

import os
import json
import requests

HCLOUD_TOKEN = os.environ['HCLOUD_TOKEN']
ip_url = "https://ipv4.icanhazip.com"

def get_public_ip():
  response = requests.get(ip_url)
  public_ip = response.text.strip()
  return public_ip

url = "https://api.hetzner.cloud/v1/firewalls/{id}}"
headers = {"content-type": "application/json; charset=UTF-8",'Authorization':'Bearer {}'.format(HCLOUD_TOKEN)}
response = requests.get(url, headers=headers).json()

rules = response['firewall']['rules']
new = {'direction': 'in', 'protocol': 'tcp', 'port': 'any', 'source_ips': [''+ get_public_ip() + '/32'], 'destination_ips': [], 'description': 'ALLOW_FROM_HOME'}
rules.append(new)
result = json.dumps({'rules': rules})

url = "https://api.hetzner.cloud/v1/firewalls/{id}/actions/set_rules"
response = requests.post(url, headers=headers, data=result).json()
print (response)
