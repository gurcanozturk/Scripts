#!/usr/bin/env python
from pyzabbix import ZabbixAPI

def create_host(hostname, ip_addr, group_id, template_id, status):
    zapi = ZabbixAPI(API_URL)
    zapi.login(user=_API_USER, password=API_PASSWORD)
    create_host = zapi.host.create(
        host= hostname,
        status= status,
        interfaces=[{
            "type": 1,
            "main": "1",
            "useip": 1,
            "ip": ip_addr,
            "dns": "",
            "port": 10050
        }],
        groups=[{
            "groupid": group_id
        }],
        templates=[{
            "templateid": template_id
        }]
    )

API_URL="zabbix_server_IP"
API_USER="apiuser"
API_PASSWORD="api_password"

AGENT_GROUPID=2
AGENT_TEMPLATEID=10001
AGENT_STATUS=1

d = { line.strip().split(":")[0] : line.strip().split(":")[1] for line in open("host.list") }

for AGENT_HOSTNAME, AGENT_IP in d.items():
  create_host(AGENT_HOSTNAME, AGENT_IP, AGENT_GROUPID, AGENT_TEMPLATEID, AGENT_STATUS)